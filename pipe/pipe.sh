#!/bin/sh
#
# This pipe is an example to show how easy is to create pipes for Bitbucket Pipelines.
#


# Required parameters
# WIDOW_HOST=${WIDOW_HOST:?'WIDOW_HOST missing'}
# WIDOW_USER=${WIDOW_USER:?'WIDOW_USER missing'}
# WIDOW_PW=${WIDOW_PW:?'WIDOW_PW missing'}
VERSION=${VERSION:?"VERSION missing"}
PROJECT_NAME=${PROJECT_NAME:?"PROJECT_NAME missing"}

# Default parameters
pip install awscli
aws codebuild start-build --project-name ${PROJECT_NAME} --source-version ${VERSION}
  # --environment-variables-override name=WIDOW_HOST,value=${WIDOW_HOST},type=PLAINTEXT name=WIDOW_USER,value=${WIDOW_USER},type=PLAINTEXT name=WIDOW_PW,value=${WIDOW_PW},type=PLAINTEXT