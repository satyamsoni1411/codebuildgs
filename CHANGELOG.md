# Changelog

Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.0

- minor: Initial release

## 0.1.1

- minor: Fixed security vulnerability
